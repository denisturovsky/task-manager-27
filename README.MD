# TASK MANAGER

## DEVELOPER INFO

* NAME: Denis Turovksy

* E-MAIL: dturovsky@t1-consulting.ru

* E-MAIL: shveikko@gmail.com

## SOFTWARE

* OpenJDK 8

* IntelliJ Idea

* Windows 10 Pro Version 20H2 Build 19042.985

## HARDWARE

* RAM: 16Gb

* CPU: i5

* HDD: 512Gb

## BUILD APPLICATION

```bash
mvn clean install
```

## RUN PROGRAM

```bash
java -jar task-manager.jar
```
